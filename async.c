#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <libpq-fe.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <threads.h>
#include <unistd.h>

static int   port;
static char* database;

static const int  backlog     = 511;  // like in nginx
static const char req_tail[]  = "\r\n\r\n";
static const char query[]     = "SELECT PG_SLEEP(5)";
static const char ok_reply[]  = "HTTP/1.0 204 No Content\r\n\r\n";
static const char bad_reply[] = "HTTP/1.0 500 Internal Server Error\r\n\r\n";

static void set_nonblocking(int fd)
{
    const int flags = fcntl(fd, F_GETFL, 0);
    if(flags < 0)
    {
        perror("fcntl");
        _exit(EXIT_FAILURE);
    }
    int rc = fcntl(fd, F_SETFL, flags | O_NONBLOCK);
    if(rc < 0)
    {
        perror("fcntl");
        _exit(EXIT_FAILURE);
    }
}

static int worker(void*)
{
    int serverfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(serverfd < 0)
    {
        perror("socket");
        _exit(EXIT_FAILURE);
    }

    set_nonblocking(serverfd);

    // load balancer курильщика
    if(setsockopt(serverfd, SOL_SOCKET, SO_REUSEPORT, &(int){1}, sizeof(int)) <
       0)
    {
        perror("setsockopt");
        _exit(EXIT_FAILURE);
    }

    struct sockaddr_in sa = {.sin_family      = AF_INET,
                             .sin_port        = htons(port),
                             .sin_addr.s_addr = htonl(INADDR_ANY)};
    if(bind(serverfd, (struct sockaddr*)&sa, sizeof(sa)) < 0)
    {
        perror("bind");
        close(serverfd);
        _exit(EXIT_FAILURE);
    }

    if(listen(serverfd, backlog) < 0)
    {
        perror("listen");
        close(serverfd);
        _exit(EXIT_FAILURE);
    }

    int epollfd = epoll_create1(0);
    if(epollfd < 0)
    {
        perror("epoll");
        _exit(EXIT_FAILURE);
    }

    struct epoll_event ev = {.events = EPOLLIN, .data.fd = serverfd};
    if(epoll_ctl(epollfd, EPOLL_CTL_ADD, serverfd, &ev) < 0)
    {
        perror("epoll_ctl serverfd");
        _exit(EXIT_FAILURE);
    }

    PGconn* dbconn_pool[1024] = {0};
    bool    query_sent[1024]  = {0};  // HACK, should use epoll_data_t

    for(;;)  // ever
    {
        struct epoll_event events[1024];

        int nfds = epoll_wait(epollfd, events,
                              sizeof(events) / sizeof(struct epoll_event), -1);
        if(nfds < 0)
        {
            perror("epoll_wait");
            _exit(EXIT_FAILURE);
        }
        for(int n = 0; n < nfds; n++)
        {
            if(events[n].data.fd == serverfd)
            {
                int clientfd = accept(serverfd, NULL, NULL);
                if(clientfd < 0)
                {
                    perror("accept");
                    close(serverfd);
                    _exit(EXIT_FAILURE);
                }
                set_nonblocking(clientfd);
                ev.events  = EPOLLIN | EPOLLET;
                ev.data.fd = clientfd;
                if(epoll_ctl(epollfd, EPOLL_CTL_ADD, clientfd, &ev) < 0)
                {
                    perror("epoll_ctl accept");
                    _exit(EXIT_FAILURE);
                }
                query_sent[clientfd] = false;
            }
            else
            {
                int clientfd = events[n].data.fd;
                if(!query_sent[clientfd])
                {
                    char    buffer[256] = {0};
                    ssize_t size = 0, r = 0;
                    for(;;)
                    {
                        size += r;
                        r = read(clientfd, buffer + size,
                                 sizeof(buffer) - size);
                        if(r == 0)
                        {
                            // connection was closed
                            size = 0;
                            break;
                        }
                        if(r < 0)
                        {
                            if(errno != EAGAIN)
                            {
                                perror("read");
                                size = 0;  // read broken, skip processing
                            }
                            break;
                        }
                    }

                    if(size)
                    {
                        if((size_t)size > sizeof(req_tail) &&
                           strcmp(buffer + size - sizeof(req_tail) + 1,
                                  req_tail) == 0)
                        {
                            PGconn* dbconn = dbconn_pool[clientfd];
                            if(!dbconn)
                            {
                                dbconn = dbconn_pool[clientfd] =
                                    PQconnectdb(database);
                                if(PQstatus(dbconn) != CONNECTION_OK)
                                {
                                    puts(PQerrorMessage(dbconn));
                                    PQfinish(dbconn);
                                    dbconn_pool[clientfd] = NULL;
                                    ssize_t r = write(clientfd, bad_reply,
                                                      sizeof(bad_reply) - 1);
                                    if(r < 0)
                                        perror("write");
                                    goto close_conn;
                                }
                            }
                            if(!PQsendQuery(dbconn, query))
                            {
                                puts(PQerrorMessage(dbconn));
                                ssize_t r = write(clientfd, bad_reply,
                                                  sizeof(bad_reply) - 1);
                                if(r < 0)
                                    perror("write");
                                goto close_conn;
                            }
                            query_sent[clientfd] = true;
                            ev.events            = EPOLLIN | EPOLLET;
                            ev.data.fd           = clientfd;
                            if(epoll_ctl(epollfd, EPOLL_CTL_ADD,
                                         PQsocket(dbconn), &ev) < 0)
                            {
                                perror("epoll_ctl");
                                _exit(EXIT_FAILURE);
                            }
                        }
                        else
                        {
                            // no request tail yet
                        }
                        continue;
                    }
                }
                else
                {
                    PGconn* dbconn = dbconn_pool[clientfd];
                    if(!PQconsumeInput(dbconn))
                    {
                        puts(PQerrorMessage(dbconn));
                        ssize_t r =
                            write(clientfd, bad_reply, sizeof(bad_reply) - 1);
                        if(r < 0)
                            perror("write");
                        goto close_conn;
                    }
                    if(PQisBusy(dbconn))
                        continue;
                    if(epoll_ctl(epollfd, EPOLL_CTL_DEL, PQsocket(dbconn),
                                 NULL) < 0)
                        perror("epoll_ctl");
                    PGresult* result = PQgetResult(dbconn);
                    if(PQresultStatus(result) != PGRES_TUPLES_OK)
                    {
                        // unexpected reply
                        PQclear(result);
                        ssize_t r =
                            write(clientfd, bad_reply, sizeof(bad_reply) - 1);
                        if(r < 0)
                            perror("write");
                        goto close_conn;
                    }
                    while(result)
                    {
                        PQclear(result);
                        result = PQgetResult(dbconn);
                    }

                    ssize_t r = write(clientfd, ok_reply, sizeof(ok_reply) - 1);
                    if(r < 0)
                        perror("write");
                }

            close_conn:
                if(epoll_ctl(epollfd, EPOLL_CTL_DEL, clientfd, NULL) < 0)
                    perror("epoll_ctl remove");
                if(shutdown(clientfd, SHUT_RDWR) < 0)
                    perror("shutdown");
                close(clientfd);
                query_sent[clientfd] = false;
            }
        }
    }
}

int main(int argc, char** argv)
{
    if(argc != 4)
    {
        printf("USAGE: %s <nthreads> <port> <database>", argv[0]);
        return EXIT_FAILURE;
    }

    signal(SIGPIPE, SIG_IGN);

    port     = atoi(argv[2]);
    database = argv[3];

    int    nthreads = atoi(argv[1]);
    thrd_t thread;
    for(int i = 0; i < nthreads; i++)
        thrd_create(&thread, worker, NULL);

    int res;
    thrd_join(thread, &res);
    return EXIT_SUCCESS;
}
