#include <arpa/inet.h>
#include <libpq-fe.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <threads.h>
#include <unistd.h>

static int   port;
static char* database;

static const int  backlog     = 511;  // like in nginx
static const char req_tail[]  = "\r\n\r\n";
static const char query[]     = "SELECT PG_SLEEP(5)";
static const char ok_reply[]  = "HTTP/1.0 204 No Content\r\n\r\n";
static const char bad_reply[] = "HTTP/1.0 500 Internal Server Error\r\n\r\n";

static int worker(void*)
{
    PGconn* dbconn = PQconnectdb(database);
    if(PQstatus(dbconn) != CONNECTION_OK)
    {
        puts(PQerrorMessage(dbconn));
        PQfinish(dbconn);
        _exit(EXIT_FAILURE);
    }

    int serverfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if(serverfd < 0)
    {
        perror("socket");
        _exit(EXIT_FAILURE);
    }

    // load balancer курильщика
    if(setsockopt(serverfd, SOL_SOCKET, SO_REUSEPORT, &(int){1}, sizeof(int)) <
       0)
    {
        perror("setsockopt");
        _exit(EXIT_FAILURE);
    }
    struct sockaddr_in sa = {.sin_family      = AF_INET,
                             .sin_port        = htons(port),
                             .sin_addr.s_addr = htonl(INADDR_ANY)};
    if(bind(serverfd, (struct sockaddr*)&sa, sizeof sa) < 0)
    {
        perror("bind");
        close(serverfd);
        _exit(EXIT_FAILURE);
    }

    if(listen(serverfd, backlog) < 0)
    {
        perror("listen");
        close(serverfd);
        _exit(EXIT_FAILURE);
    }

    for(;;)  // ever
    {
        int clientfd = accept(serverfd, NULL, NULL);
        if(clientfd < 0)
        {
            perror("accept");
            close(serverfd);
            _exit(EXIT_FAILURE);
        }

        char    buffer[256] = {0};
        ssize_t r;
    read_loop:
        if((r = read(clientfd, buffer, sizeof(buffer))) > 0)
        {
            if((size_t)r > sizeof(req_tail) &&
               strcmp(buffer + r - sizeof(req_tail) + 1, req_tail) == 0)
            {
                PGresult* query_res = PQexec(dbconn, query);
                if(PQresultStatus(query_res) != PGRES_TUPLES_OK)
                {
                    r = write(clientfd, bad_reply, sizeof(bad_reply) - 1);
                    if(r < 0)
                        perror("write");
                }
                else
                {
                    r = write(clientfd, ok_reply, sizeof(ok_reply) - 1);
                    if(r < 0)
                        perror("write");
                }
                PQclear(query_res);
            }
            else
            {
                goto read_loop;
            }
        }

        if(shutdown(clientfd, SHUT_RDWR) < 0)
            perror("shutdown");
        close(clientfd);
    }
}

int main(int argc, char** argv)
{
    if(argc != 4)
    {
        printf("USAGE: %s <nthreads> <port> <database>", argv[0]);
        return EXIT_FAILURE;
    }

    port     = atoi(argv[2]);
    database = argv[3];

    int    nthreads = atoi(argv[1]);
    thrd_t thread;
    for(int i = 0; i < nthreads; i++)
        thrd_create(&thread, worker, NULL);

    int res;
    thrd_join(thread, &res);
    return EXIT_SUCCESS;
}
